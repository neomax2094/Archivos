package source;

import java.io.File;

public class BorraArchivos extends Thread{
    private String nombre,contenido;
    public BorraArchivos(String nombre, String contenido){
        this.nombre = nombre;
        this.contenido = contenido;
    }
    public void run(){
        try {
            Thread.sleep(100);
            File file = new File(nombre);
            file.delete();
            System.out.println("Archivo: "+nombre+" borrado!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
