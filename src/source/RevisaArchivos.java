package source;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

import static source.Main.archivosEncarpeta;
import static source.Main.contenidosArchivosEnCarpeta;

public class RevisaArchivos {
    public static String revisa(){
        String nombre="";
        File tmpDir = new File("tmp/");
        File[] listed = tmpDir.listFiles();
        ArrayList<String> nombres = new ArrayList<>();
        for (File f : listed){
            archivosEncarpeta.add("tmp/"+f.getName());
            nombres.add(f.getName());
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader("tmp/"+f.getName()));
                for (String line; (line = bufferedReader.readLine()) != null;) {
//                    nombres.add(line);
                }
                bufferedReader.close();
                Random random = new Random();
                nombre = nombres.get(random.nextInt(nombres.size()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return nombre;
    }
}
