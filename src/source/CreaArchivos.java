package source;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class CreaArchivos extends Thread {
    private String nombre;
    private String contenido;
    CreaArchivos(String nombre, String contenido){
        this.nombre=nombre;
        this.contenido=contenido;
    }
    public void run(){
        BufferedWriter bufferedWriter = null;
        FileWriter fileWriter = null;
        try {
            Thread.sleep(100);
            fileWriter = new FileWriter(nombre);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(contenido);
            System.out.println("Archivo: "+nombre+" creado con: "+contenido);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (bufferedWriter!=null){
                try {
                    bufferedWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileWriter!=null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
