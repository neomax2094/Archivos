package source;

import java.util.ArrayList;
import java.util.Random;
import static source.Main.contenidosArchivosEnCarpeta;
import static source.Main.archivosEncarpeta;
import static source.Main.nuevosContenidos;
public class HiloOrquestador extends Thread {
    Random random = new Random();
    public void run(){
        Random random = new Random();
        ArrayList<String> nombres = new ArrayList<>();
        ArrayList<String> contenidos = new ArrayList<>();
        ArrayList<CreaArchivos> creaArchivos = new ArrayList<>();
        ArrayList<BorraArchivos> borraArchivos = new ArrayList<>();
        for (int i=0; i<10; i++){
            nombres.add("tmp/" +i+"-"+ String.valueOf((random.nextInt(100000000) + 100000000)) + ".tx");
        }
        for (int i=0; i<10; i++){
            contenidos.add(String.valueOf( (random.nextInt(1522) + random.nextInt(1987)) +"."+ (random.nextInt(98756)) ));
        }
        for (int i=0; i<10; i++){
            creaArchivos.add(new CreaArchivos(nombres.get(i),contenidos.get(i)));
        }
        for (int i=0; i<10; i++){
            creaArchivos.get(i).run();
        }
        System.out.println("Archivos creados!");
//        System.out.println("Valide los archivos  precione enter para continuar!");
//        new java.util.Scanner(System.in).nextLine();
//        EnlistaArchivos.enlista();
//        System.out.println("Archsivos enlistados!");
//        ArrayList<EditaArchivos> editaArchivos = new ArrayList<>();
//        for (int i=0; i<archivosEncarpeta.size(); i++){
//            nuevosContenidos.add(String.valueOf(random.nextInt(4000)));
//        }
//        for (int i=0; i<archivosEncarpeta.size(); i++){
//            editaArchivos.add(new EditaArchivos(archivosEncarpeta.get(i),contenidosArchivosEnCarpeta.get(i),nuevosContenidos.get(i)));
//        }
//        for (int i=0; i<editaArchivos.size(); i++) {
//            String[] nuevocontenido = editaArchivos.get(i).muestraPropiedades();
//            System.out.println("Archivo: " + nuevocontenido[0] + " con contenido: " + nuevocontenido[1]);
//            editaArchivos.get(i).run();
//        }
//        System.out.println("Valide los archivos  precione enter para continuar!");
//        new java.util.Scanner(System.in).nextLine();
//        System.out.println("Borrando archivos");
//        for (int i=0; i<archivosEncarpeta.size(); i++){
//            borraArchivos.add(new BorraArchivos(archivosEncarpeta.get(i),nuevosContenidos.get(i)));
//        }
//        for (int i=0; i<borraArchivos.size(); i++){
//            borraArchivos.get(i).run();
//        }
        RevisaArchivos revisaArchivos = new RevisaArchivos();
        while (revisaArchivos.revisa()!=""){
            CreaArchivos creaArchivos_ultimate = null;
            RevisaArchivos revisaArchivos_ultimate = null;
            LeeArchivos leeArchivos_ultimate = new LeeArchivos();
            EditaArchivos editaArchivos = null;
            BorraArchivos borraArchivos_ultimate = null;
            int haz = random.nextInt(3);
//            System.out.println("haz: "+haz);
            switch (haz){
                case 0:
                    creaArchivos_ultimate = new  CreaArchivos("tmp/"+String.valueOf(random.nextInt(10000)+10000), String.valueOf(random.nextInt(10000)+10000) );
                    creaArchivos_ultimate.run();
                    break;
                case 1:
                    revisaArchivos_ultimate = new RevisaArchivos();
                    String nombre_1 = revisaArchivos_ultimate.revisa();
                    editaArchivos = new EditaArchivos("tmp/"+nombre_1,leeArchivos_ultimate.lee(nombre_1), String.valueOf(random.nextInt(10000))+"."+String.valueOf(random.nextInt(10000)) );
                    editaArchivos.run();
                    break;
                case 2:
                    revisaArchivos_ultimate = new RevisaArchivos();
                    String nombre_2 = revisaArchivos_ultimate.revisa();
                    borraArchivos_ultimate = new BorraArchivos("tmp/"+nombre_2,leeArchivos_ultimate.lee(nombre_2));
                    borraArchivos_ultimate.run();
                    break;
            }
        }
    }
}
