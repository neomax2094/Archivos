package source;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import static source.Main.cantidadArchivos;
import static source.Main.archivosEncarpeta;
import static source.Main.contenidosArchivosEnCarpeta;
public class EnlistaArchivos {
    public static void enlista(){
        File tmpDir = new File("tmp/");
        File[] listed = tmpDir.listFiles();
        for (File f : listed){
            archivosEncarpeta.add("tmp/"+f.getName());
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader("tmp/"+f.getName()));
                for (String line; (line = bufferedReader.readLine()) != null;) {
                    contenidosArchivosEnCarpeta.add(line);
                }
                bufferedReader.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        }
    }
