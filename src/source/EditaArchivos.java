package source;

import java.io.*;

public class EditaArchivos extends Thread{
    private String nombre,contenido,nuevoContenido;
    public String nombreOut,nuevoContenidoOut;
    EditaArchivos(String nombre,String contenido, String nuevoContenido){
        this.nombre=nombre;
        nombreOut=nombre;
        this.contenido=contenido;
        this.nuevoContenido=nuevoContenido;
        nuevoContenidoOut=nuevoContenido;
    }
    public void run(){File fileToBeModified = new File(nombre);
        String oldContent = "";
        BufferedReader bufferedReader = null;
        FileWriter fileWriter = null;
        try{
            Thread.sleep(100);
            bufferedReader = new BufferedReader(new FileReader(fileToBeModified));
            String line = bufferedReader.readLine();
            while (line != null){
                oldContent = oldContent + line + System.lineSeparator();
                line = bufferedReader.readLine();
            }
            String newContent = oldContent.replaceAll(contenido, nuevoContenido);
            fileWriter = new FileWriter(fileToBeModified);
            fileWriter.write(newContent);
            System.out.println("Archivo: "+nombre+" tenia: "+contenido+" editado a: "+nuevoContenido);
            bufferedReader.close();
            fileWriter.close();
        } catch (FileNotFoundException e){
//            System.out.println(nombre+" "+contenido+" "+nuevoContenido);
        } catch (IOException e){
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public String[] muestraPropiedades(){
        String[] out = new String[2];
        out[0]=nombreOut;
        out[1]=nuevoContenidoOut;
        return out;
    }
}
